import React from 'react';
import {Pet} from "./Pet";
import PropTypes from 'prop-types';

export const ListPet = ({appointments, deleteAppointment}) => {

    return (

        <>

            {appointments.map(data => (
                <Pet key={data.id} data={data} deleteAppointment={deleteAppointment}/>
                // <Pet key={data.id} {...data} deleteAppointment={deleteAppointment}/>
            ))}

        </>

    )
};

ListPet.propTypes = {
    appointments : PropTypes.array.isRequired,
    deleteAppointment : PropTypes.func.isRequired
}