import React from 'react';
import PropTypes from 'prop-types';

export const Pet = ({data,  deleteAppointment}) => {

    const {id, pet, ownerPet, date, time, symptom} = data;

    return (

        <div className="appointment">
            <p>Mascota : <span>{pet}</span></p>
            <p>Owner : <span>{ownerPet}</span></p>
            <p>Symptom : <span>{symptom}</span></p>
            <p>Date : <span>{date}</span></p>
            <p>Time : <span>{time}</span></p>

            <button className="button delete u-full-width"
                    onClick={() => deleteAppointment(id)}>Delete &times;</button>

        </div>

    )
};

Pet.propTypes = {
    data: PropTypes.object.isRequired,
    deleteAppointment: PropTypes.func.isRequired,
}
