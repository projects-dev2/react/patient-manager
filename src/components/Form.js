import React, {useState} from 'react';
import {useForm} from "../hooks/useForm";
import uniqid from 'uniqid';
import PropTypes from 'prop-types';

export const Form = ({addAppointment}) => {

    const initialState = {
        pet: '',
        ownerPet: '',
        date: '',
        time: '',
        symptom: '',
        id: uniqid.process()
    };

    const [value, handleInputChange, reset] = useForm(initialState);
    const {pet, ownerPet, date, time, symptom} = value;
    const [error, setError] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();

        // to Validate data
        if (pet.trim() === '' || ownerPet.trim() === '' || date.trim() === '' || time.trim() === '' || symptom.trim() === '') {
            setError(true);
            return 1;     // Exit of submit
        }

        setError(false);

        // to create and save addAppointment
        addAppointment(value);

        // reset
        reset(initialState);
    }

    return (

        <>
            <h2>To create appointment</h2>

            {error &&
            <p className="alert-error">All fields are required</p>
            }

            <form onSubmit={handleSubmit}>

                <label>Pet Name</label>
                <input type="text"
                       name="pet"
                       className="u-full-width"
                       placeholder="Pet name"
                       onChange={handleInputChange}
                       value={pet}
                />

                <label>Owner Name</label>
                <input type="text"
                       name="ownerPet"
                       className="u-full-width"
                       placeholder="Owner name's pet"
                       onChange={handleInputChange}
                       value={ownerPet}
                />

                <label>Date</label>
                <input type="date"
                       name="date"
                       className="u-full-width"
                       onChange={handleInputChange}
                       value={date}
                />

                <label>Out time</label>
                <input type="time"
                       name="time"
                       className="u-full-width"
                       onChange={handleInputChange}
                       value={time}
                />

                <label>Symptom</label>
                <textarea className="u-full-width"
                          name="symptom"
                          onChange={handleInputChange}
                          value={symptom}
                > </textarea>

                <button type="submit"
                        className="u-full-width button-primary"
                >
                    To add appointment
                </button>

            </form>
        </>

    )
};

Form.propTypes = {
    addAppointment : PropTypes.func.isRequired
}