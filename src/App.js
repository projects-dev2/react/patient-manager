import {Form} from "./components/Form";
import {ListPet} from "./components/ListPet";
import {useEffect, useState} from "react";

function App() {

    let appointmentsLocal = JSON.parse(localStorage.getItem('appointments'));

    if (!appointmentsLocal) {
        appointmentsLocal = [];
    }

    const [appointments, setAppointments] = useState(appointmentsLocal);

    const addListAppointment = (data) => {

        setAppointments([
            ...appointments,
            data
        ]);

    }

    const deleteAppointment = id => {
        setAppointments(appointments.filter(appointment => appointment.id !== id));
    }

    useEffect(() => {

        appointments ?
            localStorage.setItem('appointments', JSON.stringify(appointments)) :
            localStorage.setItem('appointments', []);

    }, [appointments]);

    const title = appointments.length === 0 ? `You've no records` : 'Management your appointment';

    return (
        <div className="">
            <h1>Patient Manager</h1>

            <div className="container">
                <div className="row">
                    <div className="one-half column">
                        <Form addAppointment={addListAppointment}/>
                    </div>
                    <div className="one-half column">
                        <h2>{title}</h2>
                        {appointments.length !== 0 &&
                        <ListPet appointments={appointments} deleteAppointment={deleteAppointment}/>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
